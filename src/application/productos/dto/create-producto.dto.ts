import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateProductoDto {
  @ApiProperty({ example: "A-001" })
  @IsNotEmpty()
  codigo: string;

  @ApiProperty({ example: "Nombre del producto" })
  @IsNotEmpty()
  nombre: string;

  @ApiProperty({ example: "Categoria del producto" })
  @IsNotEmpty()
  categoria: string;

  @ApiProperty({ example: "12.99" })
  @IsNotEmpty()
  precio: number;
}

export class RespuestaCrearProductoDto {
  @ApiProperty({ example: "1" })
  @IsNotEmpty()
  id: string;
}
