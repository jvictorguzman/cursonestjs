import { Module } from "@nestjs/common";
import { ProductosService } from "./service/productos.service";
import { ProductosController } from "./controller/productos.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Producto } from "./entities/producto.entity";

@Module({
  controllers: [ProductosController],
  providers: [ProductosService],
  imports: [TypeOrmModule.forFeature([Producto])],
  exports: [ProductosService],
})
export class ProductosModule {}
